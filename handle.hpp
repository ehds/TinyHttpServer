#include "server_base.hpp"
#include <fstream>
using namespace std;
using namespace ShiyanlouWeb;


template<typename SERVER_TYPE>
void start_server(SERVER_TYPE &server){

	server.resource["^/string/?$"]["POST"] = [](ostream& response, Request & request){
		stringstream ss;
		*request.content >> ss.rdbuf();
		string content = ss.str();
		response << "HTTP/1.1 200 OK\r\nContent-Length: " << content.length() << "\r\n\r\n" << content;
	};
		server.resource["^/info/?$"]["GET"] = [](ostream& response, Request& reques){
			stringstream content_stream;
			content_stream << "<h1>Request:</h1>";
			content_stream << reques.method << " " << reques.path << "HTTP/" << reques.http_version << "<br>";
			for (auto & header : reques.header){
				content_stream << header.first << ":" << header.second << "<br>";

			}
			content_stream.seekp(0, ios::end);
			response << "HTTP/1.1 200 OK\r\nContent-Length" << content_stream.tellp() << "\r\n\r\n" << content_stream.rdbuf();
		};


		server.resource["^/match/([0-9a-zA-Z]+)/?$"]["GET"] = [](ostream& response, Request& request) {
			string number = request.path_match[1];
			response << "HTTP/1.1 200 OK\r\nContent-Length: " << number.length() << "\r\n\r\n" << number;
		};

		//处理默认的GET请求，如果没有其他匹配成功，则这个匿名函数会被调用
		//默认为/web/index.html

		server.default_resource["^/?(.*)$"]["GET"] = [](ostream& response, Request& request){
			string filename = "web/";
			string path = request.path_match[1];
			//防止使用‘..’来hack
			//rfind 找到最后一个出现.的位置
			size_t last_post = path.rfind(".");
			size_t current_pos = 0;
			size_t pos;
			while ((pos = path.find('.', current_pos)) != string::npos && pos != last_post){
				current_pos = pos;
				//把有'.'的字符全部移除只保留最后一个
				path.erase(pos, 1);
				last_post--;
			}
			filename += path;
			ifstream ifs;
			//简单的平台无关的文件或目录检查
			if (filename.find('.') == string::npos){
				if (filename[filename.length() - 1] != '/')
				{
					filename += '/';
					filename += "index.html";
				}
				ifs.open(filename, ifstream::in);

				if (ifs){
					ifs.seekg(0, ios::end);//移到输入流末尾
					size_t length = ifs.tellg();//获得输入流的大小
					ifs.seekg(0, ios::beg);//移到输入流的开始位置
					response << "HTTP/1.1 200 OK\r\nContent-Length: " << length << "\r\n\r\n" << ifs.rdbuf();
					ifs.close();


				}

			}
			else
			{
				string content = "Could not open file" + filename;
				response << "HTTP/1.1 400 Bad Request\r\nContent-Length: " << content.length() << "\r\n\r\n" << content;
			}


		};
		server.start();

	


}